import 'package:expensetracker/expense_items.dart';
import 'package:flutter/material.dart';
import 'package:expensetracker/tracker.dart';

class ExpenseList extends StatelessWidget {
  const ExpenseList(
      {super.key, required this.requiredList, required this.onRemoveExpense});
  final List<Expense> requiredList;
  final void Function(Expense expense) onRemoveExpense;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: requiredList.length,
      itemBuilder: (cntxt, lstindex) => Dismissible(
        background: Container(
          color: Theme.of(context).colorScheme.error,
          margin: EdgeInsets.symmetric(
              horizontal: Theme.of(context).cardTheme.margin!.horizontal),
        ),
        key: ValueKey(requiredList[lstindex]),
        onDismissed: (direction) => onRemoveExpense(requiredList[lstindex]),
        child: ExpenseItems(
          requiredList[lstindex],
        ),
      ),
    );
  }
}
