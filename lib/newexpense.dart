import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:expensetracker/tracker.dart';
import 'package:flutter/widgets.dart';

class NewExpense extends StatefulWidget {
  const NewExpense({super.key, required this.onaddMethod});
  final Function(Expense expense) onaddMethod;
  @override
  State<NewExpense> createState() {
    return _NewExpenseState();
  }
}

class _NewExpenseState extends State<NewExpense> {
  final _txtEditingConroller = TextEditingController();
  final _ammntController = TextEditingController();
  Category showcontent = Category.food;

  DateTime? currentdate;

  @override
  void dispose() {
    _txtEditingConroller.dispose();
    _ammntController.dispose();

    super.dispose();
  }

  void _setExpenses() {
    final amountExpense = double.tryParse(_ammntController.text);
    // ignore: non_constant_identifier_names
    final InvalidExpenses = amountExpense == null || amountExpense <= 0;
    if (InvalidExpenses ||
        _ammntController.text.trim().isEmpty ||
        currentdate == null) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text("Invalid input"),
          content: const Text(
              "Please make sure valid title , amount , date and category was entered!"),
          actions: [
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: const Text("Okay"))
          ],
        ),
      );
      return;
    }
    widget.onaddMethod(Expense(
      title: _txtEditingConroller.text,
      amount: amountExpense,
      date: currentdate!,
      category: showcontent,
    ));
    //.....
  }

  void _dateFormatted() async {
    final firstDate = DateTime.now();
    final now = DateTime(firstDate.year - 1, firstDate.month, firstDate.day);

    final date = await showDatePicker(
        context: context, firstDate: now, lastDate: firstDate);
    setState(() {
      currentdate = date;
    });
  }

  void exitinfo() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    final addValue = MediaQuery.of(context).viewInsets.bottom;
    // return LayoutBuilder(builder: (ctx, constraints) {
    //   var width = constraintsonPressed.maxWidth;
    return Padding(
        padding: EdgeInsets.fromLTRB(16, 16, 16, addValue + 16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _txtEditingConroller,
                      maxLength: 50,
                      decoration: const InputDecoration(
                        label: Text("Title"),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: TextField(
                        controller: _ammntController,
                        maxLength: 10,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            prefixText: '\$ ', label: Text("amount")),
                      ),
                    ),
                    const SizedBox(
                      width: 46,
                    ),
                    Expanded(
                        child: Row(
                      children: [
                        Text(
                          currentdate == null
                              ? 'No date selected'
                              : formatter.format(currentdate!),
                        ),
                        IconButton(
                            onPressed: _dateFormatted,
                            icon: const Icon(Icons.calendar_month_sharp)),
                      ],
                    ))
                  ]),
              Row(
                children: [
                  DropdownButton(
                      value: showcontent,
                      items: Category.values
                          .map((category) => DropdownMenuItem(
                              value: category,
                              child: Text(category.name.toUpperCase())))
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          showcontent = value!;
                        });
                      }),
                  const SizedBox(
                    width: 70,
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('Cancel'),
                        ),
                        ElevatedButton(
                            onPressed: _setExpenses,
                            child: const Text('Save Expense'))
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
