import 'package:expensetracker/chart.dart';
import 'package:expensetracker/expenses_list.dart';
import 'package:expensetracker/newexpense.dart';
import 'package:flutter/material.dart';
import 'package:expensetracker/tracker.dart';

class ExpenseTracker extends StatefulWidget {
  const ExpenseTracker({super.key});

  @override
  State<ExpenseTracker> createState() {
    return _ExpenseTrackerState();
  }
}

class _ExpenseTrackerState extends State<ExpenseTracker> {
  final List<Expense> list = [
    Expense(
        title: 'Hacking Courses ',
        amount: 15,
        date: DateTime.now(),
        category: Category.leisure),
    Expense(
        title: 'Placement Course  ',
        amount: 75,
        date: DateTime.now(),
        category: Category.work),
    Expense(
        title: 'Pizza Expenses',
        amount: 45,
        date: DateTime.now(),
        category: Category.food),
  ];
  void _openAddExpenseOverlay() {
    showModalBottomSheet(
      useSafeArea: true,
      isScrollControlled: true,
      context: context,
      builder: (contxt) => NewExpense(onaddMethod: addExpenses),
    );
  }

  void addExpenses(Expense expense) {
    setState(() {
      list.add(expense);
    });
  }

  void onDismissedExpense(Expense expense) {
    final deletedIndex = list.indexOf(expense);
    setState(() {
      list.remove(expense);
    });
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 3),
        content: const Text('Expense deleted'),
        action: SnackBarAction(
          label: 'undo',
          onPressed: () {
            setState(() {
              list.insert(deletedIndex, expense);
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    Widget maincontent = const Center(
      child: Text("No expenses found.Start adding some!"),
    );
    if (list.isNotEmpty) {
      maincontent = ExpenseList(
        requiredList: list,
        onRemoveExpense: onDismissedExpense,
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Flutter Expensetracker"),
        actions: [
          IconButton(
            onPressed: _openAddExpenseOverlay,
            icon: const Icon(Icons.add),
            // style: IconButton.styleFrom(
            //   foregroundColor: Colors.black,
            //   backgroundColor: Colors.purple,
            // ),
          ),
        ],
      ),
      body: width < 500
          ? Column(
              children: [
                Chart(expenses: list),
                Expanded(
                  child: maincontent,
                ),
              ],
            )
          : Row(
              children: [
                Expanded(
                  child: Chart(expenses: list),
                ),
                Expanded(
                  child: maincontent,
                ),
              ],
            ),
    );
  }
}
